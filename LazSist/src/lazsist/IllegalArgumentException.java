package lazsist;

/**
 * illegal argument exception
 * 
 * @author karolis
 * 
 */
public class IllegalArgumentException extends Exception {

	private static final long serialVersionUID = -5598006663835527422L;

	public IllegalArgumentException() {
	}

	public IllegalArgumentException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public IllegalArgumentException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public IllegalArgumentException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public IllegalArgumentException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
