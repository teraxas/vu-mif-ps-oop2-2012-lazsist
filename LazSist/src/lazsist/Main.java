package lazsist;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import lazsist.v3.LazybSistV3;
import lazsist.v3.graphics.MainWindowV3;

/**
 * 
 * @author karolis
 * 
 */
public class Main {

	/**
	 * saved data is saved here
	 */
	final static String savedData = "laSysData.ser";

	/**
	 * main method
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.print("Lažybų sistema\n");

		// NEW LazybSist
		// LazybSist laSystem = new LazybSist();
		// LazybSistV2 laSystem = new LazybSistV2();
		LazybSistV3 laSystem = null;

		try {
			ObjectInputStream in = null;

			File input = new File(savedData);

			if (input.exists()) {

				try {
					in = new ObjectInputStream(new BufferedInputStream(
							new FileInputStream(input)));
					LazybSistV3 tmp = (LazybSistV3) in.readObject();
					laSystem = tmp;

				} finally {
					in.close();
				}
				;
			} else
				laSystem = new LazybSistV3();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		// INTERFACE
		// MainWindowV2 mainWindow = new MainWindowV2(laSystem);
		MainWindowV3 mainWindow = new MainWindowV3(laSystem);

		/*
		 * if (testMode) { laSystem.test(); System.out.println("Test Mode: ON");
		 * }
		 */

		mainWindow.setVisible(true);

		// COLLECTIONS
		// BetsSetTest bct = new BetsSetTest(laSystem);
		// bct.printBetsAllEvents();
	}
}