package lazsist.v1;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Bet class
 * 
 * @author karolis
 * 
 */
@XmlRootElement(name = "Bet")
public class Bet implements Comparable<Bet>, Serializable {

	private static final long serialVersionUID = -5872584394588547371L;

	/**
	 * first name
	 */
	private String fName;

	/**
	 * last name
	 */
	private String lName;

	/**
	 * bet id
	 */
	private int id;

	/**
	 * rate at the time of bet
	 */
	private double rate;

	/**
	 * bet
	 */
	private double bet;

	/**
	 * date bet was made
	 */
	private Date date;

	/**
	 * date format
	 */
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

	/**
	 * ID of event
	 */
	private int eventId;

	/**
	 * team index
	 */
	private int teamIdx;

	// private final String gameType = "Lazybos";

	/**
	 * was bet won?
	 */
	private boolean wonBet = false;

	/**
	 * constructor
	 * 
	 * @param fName first name
	 * @param lName last name
	 * @param eventId event ID
	 * @param rate current rate of team
	 * @param bet size of bet
	 * @param teamIdx team index
	 */
	public Bet(String fName, String lName, int eventId, double rate,
			double bet, int teamIdx) {
		this.fName = fName;
		this.lName = lName;
		this.rate = rate;
		this.bet = bet;
		this.teamIdx = teamIdx;
		this.eventId = eventId;
		this.date = new Date();
	}

	/**
	 * default constructor
	 */
	public Bet() {
		this("First", "Last", -1, 0, 0, -1);
	}

	/**
	 * compare to another Bet
	 * 
	 * @param o object
	 * @return compare result
	 */
	@Override
	public int compareTo(Bet o) {
		if (this.lName.compareTo(o.lName) > 0)
			return 1;
		if (this.lName.compareTo(o.lName) < 0)
			return -1;
		return 0;
	}

	/**
	 * converts object to String
	 */
	@Override
	public String toString() {
		String tmp = dateFormat.format(date) + " " + lName + " " + fName + ", "
				+ bet + " Lt, ID: " + id + ", Laimėjimas: " + getWin();
		return tmp;
	}

	/**
	 * checks if bet is equal to another bet
	 * 
	 * @param obj object
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Bet) {
			Bet bet = (Bet) obj;
			if (this.fName.equals(bet.fName) || this.lName.equals(bet.lName)
					|| this.id == bet.id || this.rate == bet.rate
					|| this.bet == bet.bet || this.date.equals(bet.date)
					|| this.eventId == bet.eventId
					|| this.teamIdx == bet.teamIdx) {
				return true;
			} else
				return false;
		}
		return false;
	}

	// ==GETTERS===============================================
	public boolean getWinBool() {
		return wonBet;
	}

	public double getWin() {
		if (wonBet) {
			return bet * rate;
		} else
			return bet * (-1);
	}

	public int getTeamIdx() {
		return teamIdx;
	}

	public String getFName() {
		return fName;
	}

	public String getLName() {
		return lName;
	}

	public double getRate() {
		return rate;
	}

	public double getBet() {
		return bet;
	}

	public int getBetId() {
		return id;
	}

	public int getEventId() {
		return this.eventId;
	}

	// ==SETTERS==============================================
	@javax.xml.bind.annotation.XmlElement(name = "fName")
	public void setfName(String fName) {
		this.fName = fName;
	}

	@javax.xml.bind.annotation.XmlElement(name = "lName")
	public void setlName(String lName) {
		this.lName = lName;
	}

	public void setRate(double rate) {
		this.rate = rate;
	}

	public void setBet(double bet) {
		this.bet = bet;
	}

	public void setId(int betId) {
		this.id = betId;
	}

	public void setWin(boolean wonBet) {
		this.wonBet = wonBet;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public void setTeamIdx(int teamIdx) {
		this.teamIdx = teamIdx;
	}
}
