package lazsist.v1;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

import lazsist.BetException;
import lazsist.IllegalArgumentException;

/**
 * @author Karolis Jocevičius <karolis.jocevicius@gmail.com>
 * @version 0.1
 * @since 2012 12-14
 */
public class LazybSist implements Serializable {

	private static final long serialVersionUID = 2137996738582155034L;

	/**
	 * Toggles loading test data and console output
	 */
	public boolean testMode = false;

	/**
	 * Event ID for the next added event. Increases after each added event
	 */
	private int curEventId;

	/**
	 * Bet ID for the next added bet. Increases after each added bet
	 */
	private int curBetId;

	/**
	 * A list of added events
	 */
	protected ArrayList<LsEvent> events = new ArrayList<LsEvent>();

	/**
	 * A list of added bets
	 */
	protected ArrayList<Bet> bets = new ArrayList<Bet>();

	/**
	 * Default constructor
	 */
	public LazybSist() {
		curEventId = 0;
		curBetId = 0;
	}

	/**
	 * Adds event to events ArrayList
	 * 
	 * @param name event name
	 * @param loc event location
	 * @throws IllegalArgumentException if name is empty
	 */
	public void addEvent(Date date, String name, String loc)
			throws IllegalArgumentException {
		if (name.length() == 0)
			throw new IllegalArgumentException("Empty string");

		LsEvent e = new LsEvent(date, name, loc);

		curEventId++;
		e.setEventId(curEventId);
		events.add(e);
	}

	/**
	 * Adds event to events ArrayList
	 * 
	 * @param e Event
	 */
	public void addEvent(LsEvent e) {
		curEventId++;
		e.setEventId(curEventId);
		events.add(e);
	}

	/**
	 * Adds bet to bets ArrayList
	 * 
	 * @param fName first name
	 * @param lName last name
	 * @param rate current rate of team
	 * @param bet bet size 
	 * @param teamIdx team index
	 * @param eventId event ID
	 * @throws IllegalArgumentException if last of first name is empty
	 */
	public void addBet(String fName, String lName, double rate, double bet,
			int teamIdx, int eventId) throws IllegalArgumentException {
		if (fName.length() == 0 || lName.length() == 0)
			throw new IllegalArgumentException("Empty String");
		curBetId++;
		Bet tmpBet = new Bet(fName, lName, eventId, rate, bet, teamIdx);
		tmpBet.setId(curBetId);
		bets.add(tmpBet);
		getEventByIdx(getEventIdxById(tmpBet.getEventId())).incBetCount();
	}

	/**
	 * Adds bet to bets ArrayList
	 * 
	 * @param b bet to be added
	 */
	public void addBet(Bet b) {
		curBetId++;
		b.setId(curBetId);
		bets.add(b);
		getEventByIdx(getEventIdxById(b.getEventId())).incBetCount();
	}

	/**
	 * Removes event from events ArrayList
	 * 
	 * @param idx index in events ArrayList
	 * @throws BetException if events betCount > 0
	 */
	public void rmEvent(int idx) throws BetException {
		if (events.get(idx).getBetCount() > 0)
			throw new BetException("BetsCount > 0");
		events.remove(idx);
	}

	/**
	 * Removes bet from bets ArrayList
	 * 
	 * @param idx index in bets ArrayList
	 */
	public void rmBet(int idx) {
		bets.remove(idx);
	}

	/**
	 * Prints events list to standard output
	 */
	public void printEventList() {
		for (int i = 0; i < events.size(); i++) {
			System.out.println(events.toString());
		}
	}

	/**
	 * Prints bets list to standard output
	 */
	public void printBetsList() {
		for (int i = 0; i < bets.size(); i++) {
			System.out.println(bets.toString());
		}
	}

	/**
	 * finds bet by ID
	 * 
	 * @param id id of bet
	 * @return bet index, -1 if not found
	 */
	public int findBet(int id) {
		for (int i = 0; i < bets.size(); i++) {
			if (bets.get(i).getBetId() == id) {
				return i;
			}
		}
		return -1;
	}

	// ==TEST METHODS============================================
	/**
	 * Loads test data
	 */
	public void test() {
		this.testMode = true;

		try {
			addEvent(new Date(), "hahaha", "Hehehe");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		events.get(0).addTeam("Šeškai", 1.8);
		events.get(0).addTeam("Triušiai", 1.2);

		try {
			addEvent(new Date(), "hahaha2", "Hehehe2");
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		events.get(1).addTeam("Agurkėliai", 1.5);
		events.get(1).addTeam("Burokėliai", 1.2);
		events.get(1).addTeam("Pomidorai", 1.1);
		events.get(1).addTeam("Bulvės", 1.8);

		try {
			addBet("Vardenis", "Pavardenis", 1.2, 500, 0, 1);
			addBet("Petras", "Petraitis", 1.8, 1000, 1, 1);
			addBet("Onytė", "Onaitytė", 1.8, 500, 3, 2);
			addBet("Ugnė", "Čižiūtė", 1.8, 1000, 3, 2);
			addBet("Ta", "Nia", 1.5, 10000, 0, 2);
			addBet("Chuck", "Norris", 1.2, 1250, 1, 2);
			addBet("Bruce", "Lee", 1.5, 10000, 2, 1);
			addBet("Adomas", "Adomovičius", 1.5, 10000, 0, 2);

			// Illegal arguments TEST:
			// addBet("Empty", "", 1.5, 10000, 0, 2);//EXCEPTION TEST
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
	}

	// ==SETTERS==============================================
	/**
	 * Checks betters of event if they are winners
	 * 
	 * @param eventId event ID
	 */
	public void setEventWinners(int eventId) {
		int eventIdx = getEventIdxById(eventId);
		for (int i = 0; i < bets.size(); i++) {
			if (bets.get(i).getEventId() == eventId) {
				if (bets.get(i).getTeamIdx() == getEventByIdx(eventIdx)
						.getWinnerIdx()) {
					bets.get(i).setWin(true);
				}
			}
		}
	}

	// ==GETTERS===============================================
	/**
	 * @return events ArrayList
	 */
	public ArrayList<LsEvent> getEventsList() {
		return events;
	}

	/**
	 * Get event index in the events ArrayList by ID
	 * 
	 * @param id event id
	 * @return event index, -1 if not found
	 */
	public int getEventIdxById(int id) {
		int idx = -1;
		for (int i = 0; i < events.size(); i++) {
			if (id == events.get(i).getEventId()) {
				idx = i;
				break;
			}
		}
		return idx;
	}

	/**
	 * Gets event by index in events ArrayList
	 * 
	 * @param idx event index
	 * @return event at index
	 */
	public LsEvent getEventByIdx(int idx) {
		return events.get(idx);
	}

	/**
	 * Get bet by index in bets ArrayList
	 * 
	 * @param idx bet index
	 * @return bet at index
	 */
	public Bet getBetByIdx(int idx) {
		return bets.get(idx);
	}

	/**
	 * Get bets ArrayList
	 * 
	 * @return array list of bets
	 */
	public ArrayList<Bet> getBetsList() {
		return bets;
	}

	/**
	 * Get bet index by bet id
	 * 
	 * @param id bet id
	 * @return bet index, -1 if not found
	 */
	public int getBetIdxById(int id) {
		int idx = -1;
		for (int i = 0; i < bets.size(); i++) {
			if (id == bets.get(i).getBetId()) {
				idx = i;
			}
		}
		return idx;
	}

	/**
	 * Get array list of events winner bets
	 * 
	 * @param eventId event id
	 * @return arraylist of winners
	 */
	public ArrayList<Bet> getEventWinners(int eventId) {
		ArrayList<Bet> winners = new ArrayList<Bet>();
		for (int i = 0; i < bets.size(); i++) {
			if (bets.get(i).getEventId() == eventId) {
				if (bets.get(i).getWinBool()) {
					winners.add(bets.get(i));
				}
			}
		}
		return winners;
	}

}
