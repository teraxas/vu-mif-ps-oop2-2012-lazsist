package lazsist.v1;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lazsist.v3.Teams;

import com.sun.xml.bind.v2.schemagen.xmlschema.List;

/**
 * Event class
 * 
 * @author karolis
 * 
 */
@XmlRootElement(name = "LsEvent")
public class LsEvent implements Cloneable, Comparable<LsEvent>, Serializable {

	private static final long serialVersionUID = 7589768823756113332L;

	// įvykių info
	/**
	 * ID of Event
	 */
	private int eventId;

	/**
	 * date of event
	 */
	private Date eventDate;

	/**
	 * name of event
	 */
	private String eventName;

	/**
	 * location where the event takes place
	 */
	private String eventLocation;

	/**
	 * amount of bets on this event
	 */
	private int betCount = 0;

	/**
	 * ArrayList of competitors in this event
	 */
	private ArrayList<Team> teams;

	/**
	 * is winner for the event set?
	 */
	private boolean isWinnerSet;

	/**
	 * index of the winner
	 */
	private int winnerIdx;

	/**
	 * date format
	 */
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

	/**
	 * teams object for XML
	 */
	private Teams teamsWrap;

	/**
	 * constructor with no events set
	 * 
	 * @param date date of event
	 * @param name name of event
	 * @param loc location of event
	 */
	public LsEvent(Date date, String name, String loc) {
		eventDate = date;
		eventName = name;
		eventLocation = loc;
		teams = new ArrayList<Team>();
		isWinnerSet = false;
	}

	/**
	 * costructor with events set
	 * 
	 * @param date date of event
	 * @param name name of event
	 * @param loc location of event
	 * @param teams ArrayList teams of event
	 */
	public LsEvent(Date date, String name, String loc, ArrayList<Team> teams) {
		this.eventDate = date;
		this.eventName = name;
		this.eventLocation = loc;
		this.teams = teams;
		this.isWinnerSet = false;
	}

	/**
	 * default constructor
	 */
	public LsEvent() {
		this(new Date(), "Event", "Location", new ArrayList<Team>());
	}

	/**
	 * adds new team to the Event
	 * 
	 * @param name name of team
	 * @param d rate of team
	 */
	public void addTeam(String name, double d) {
		Team tmpTeam = new Team(name, d);
		teams.add(tmpTeam);
	}

	/**
	 * removes team at index
	 * 
	 * @param index index of team
	 */
	public void rmTeamAtIdx(int index) {
		teams.remove(index);
	}

	/**
	 * increments betCount
	 */
	public void incBetCount() {
		betCount++;
	}

	/**
	 * compares two events on ID's
	 * 
	 * @param o object
	 * @return 1/0/-1
	 */
	@Override
	public int compareTo(LsEvent o) {
		if (this.eventId > o.eventId)
			return 1;
		if (this.eventId < o.eventId)
			return -1;
		return 0;
	}

	/**
	 * clones Event
	 * 
	 * @return cloned Event
	 * @throws CloneNotSupportedException if clone not supported
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object clone() throws CloneNotSupportedException {
		LsEvent cloneEvent = (LsEvent) super.clone();
		cloneEvent.setEventDate(this.eventDate);
		cloneEvent.setEventName(this.eventName);
		cloneEvent.setEventLocation(this.eventLocation);

		cloneEvent.setTeams((ArrayList<Team>) this.teams.clone());

		for (Team t : cloneEvent.getTeams()) {
			t = (Team) t.clone();
		}

		return cloneEvent;
	}

	/**
	 * converts object to string
	 * 
	 * @return string
	 */
	@Override
	public String toString() {
		String tmp = "ID: " + eventId + ", Data: "
				+ dateFormat.format(eventDate) + ", Pavadinimas: " + eventName
				+ ", Vieta: " + eventLocation;
		return tmp;
	}

	/**
	 * checks if objects arte equal
	 * 
	 * @param obj Object
	 * @return true if equal
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;
		if (obj instanceof LsEvent) {
			LsEvent e = (LsEvent) obj;
			if (this.eventId == e.eventId && this.eventName.equals(e.eventName)
					&& this.eventDate.equals(e.eventDate)
					&& this.eventLocation.endsWith(e.eventLocation)
					&& this.winnerIdx == e.winnerIdx
					&& this.betCount == e.betCount
					&& this.teams.equals(e.teams)) {
				return true;
			} else {
				return false;
			}
		} else
			return false;
	}

	/**
	 * print info on the Event to System.out
	 */
	public void printInfo() {
		System.out.println(toString());
	}

	/**
	 * print teams of the Event to System.out
	 */
	public void printTeams() {
		System.out.println(teams.size());
		for (int i = 0; i < teams.size(); i++) {
			teams.get(i).printLn();
		}
	}

	// ==GETTERS===============================================
	public String getEventName() {
		return eventName;
	}

	public String getEventLocation() {
		return eventLocation;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public ArrayList<Team> getTeams() {
		return teams;
	}

	public Team getTeam(int idx) {
		if (idx < teams.size()) {
			return teams.get(idx);
		}
		return null;
	}

	public int getTeamCount() {
		return teams.size();
	}

	public boolean getWinnerIsSet() {
		return isWinnerSet;
	}

	public int getWinnerIdx() {
		if (isWinnerSet = true) {
			return winnerIdx;
		}
		return 0;
	}

	public Team getWinnerTeam() {
		if (isWinnerSet = true) {
			return teams.get(winnerIdx);
		}
		return null;
	}

	public int getEventId() {
		return eventId;
	}

	public int getBetCount() {
		return betCount;
	}

	public Teams getTeamsWrap() {
		return teamsWrap;
	}

	// ==SETTERS==============================================
	public void setEventName(String name) {
		this.eventName = name;
	}

	public void setEventLocation(String loc) {
		this.eventLocation = loc;
	}

	public void setEventDate(Date date) {
		this.eventDate = date;
	}

	public void setWinner(int winnerIdx) {
		this.winnerIdx = winnerIdx;
		isWinnerSet = true;
	}

	public void setEventId(int id) {
		this.eventId = id;
	}

	public void setTeams(ArrayList<Team> teams) {
		this.teams = teams;
	}

	public void setTeams(List teams1) {

	}

	@XmlElement(name = "teamsLs")
	public void setTeamsWrap(Teams teamsWrap) {
		this.teamsWrap = teamsWrap;

		this.teams = new ArrayList<Team>();
		this.teams.addAll(teamsWrap.getTeam());
	}

}