package lazsist.v1;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author karolis
 */
@XmlRootElement(name = "team")
public class Team implements Cloneable, Serializable {

	private static final long serialVersionUID = -439589668210356741L;

	/**
	 * name of team
	 */
	private String name;

	/**
	 * rate of event
	 */
	private double rate;

	/**
	 * bets counter
	 */
	private int betsCount;

	/**
	 * default constructor
	 */
	public Team() {
		this("team", 1);
	}

	/**
	 * constructor
	 * 
	 * @param name name of team
	 * @param rate rate of team
	 */
	public Team(String name, double rate) {
		this.name = name;
		this.rate = rate;
		this.setBetsCount(0);
	}

	/**
	 * clone team
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	/**
	 * print team info to System.out
	 */
	public void printLn() {
		System.out.printf("%s, %.2f\n", name, rate);
	}

	/**
	 * increments betCount
	 */
	public void bet() {
		this.betsCount++;
	}

	/**
	 * convert object to string
	 */
	@Override
	public String toString() {
		String ret = "Name: " + name + ", rate: " + rate;
		return ret;
	}

	// ==GETTERS==============================================
	public double getRate() {
		return rate;
	}

	public String getName() {
		return name;
	}

	public int getBetsCount() {
		return betsCount;
	}

	// ==SETTERS==============================================
	@XmlElement(name = "name")
	public void setName(String name) {
		this.name = name;
	}

	@XmlElement(name = "rate")
	public void setRate(double rate) {
		this.rate = rate;
	}

	public void setBetsCount(int betsCount) {
		this.betsCount = betsCount;
	}

	// public void setDiff(double diff){
	// this.diff = diff;
	// }
}
