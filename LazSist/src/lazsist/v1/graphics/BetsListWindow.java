package lazsist.v1.graphics;

import java.awt.ComponentOrientation;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import lazsist.v1.LazybSist;

@SuppressWarnings("serial")
public class BetsListWindow extends JFrame {
	final static boolean shouldFill = true;
	final static boolean RIGHT_TO_LEFT = false;

	protected LazybSist laSystem;

	protected JPanel panel;

	private JLabel betsListLabel = new JLabel("Pasirinkite statymą");
	private JLabel idLabel = new JLabel("Ieškoti id: ");
	private JLabel errorLabel = new JLabel();

	private DefaultListModel<String> betsListModel = new DefaultListModel<String>();
	private JList<String> betsList = new JList<String>(betsListModel);
	private JScrollPane betsScrollPane = new JScrollPane(betsList);

	private JButton editButton = new JButton("Redaguoti");
	private JButton cancelButton = new JButton("Grįžti");
	private JButton rmBetButton = new JButton("Naikinti statymą");
	private JButton searchBetButton = new JButton("Rasti");

	private JTextField idField = new JTextField();

	public BetsListWindow(LazybSist laSystem) {
		this.laSystem = laSystem;

		initList();
		initButs();
		initUI();

		add(panel);
	}

	protected void initList() {
		betsListModel.clear();
		for (int i = 0; i < laSystem.getBetsList().size(); i++) {
			betsListModel.addElement(laSystem.getBetByIdx(i).toString());
		}
	}

	protected void initButs() {
		editButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				errorLabel.setText("");
				int idx = betsList.getSelectedIndex();

				if (idx == -1) {
					errorLabel.setText("Nepasirinktas statymas");
					return;
				}

				EditBetWindow ebw = new EditBetWindow(idx, laSystem);
				ebw.setVisible(true);
				dispose();
			}
		});

		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});

		rmBetButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				errorLabel.setText("");
				int idx = betsList.getSelectedIndex();

				if (idx == -1) {
					errorLabel.setText("Nepasirinktas statymas");
					return;
				}

				laSystem.rmBet(idx);
				initList();
			}
		});

		searchBetButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				errorLabel.setText("");
				int id;

				try {
					id = Integer.parseInt(idField.getText());
				} catch (NumberFormatException nfe) {
					errorLabel.setText("Blogas ID formatas");
					return;
				}

				int idx = laSystem.getBetIdxById(id);
				if (idx == -1) {
					errorLabel.setText("Toks ID neegzistuoja");
					return;
				}

				EditBetWindow ebw = new EditBetWindow(idx, laSystem);
				ebw.setVisible(true);
				dispose();
			}
		});

	}

	protected void initUI() {
		setTitle("Statymų sąrašas");
		setSize(500, 400);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		panel = new JPanel();

		if (RIGHT_TO_LEFT) {
			panel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		}

		panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		panel.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();
		if (shouldFill) {
			// natural height, maximum width
			c.fill = GridBagConstraints.HORIZONTAL;
		}

		betsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		betsList.setLayoutOrientation(JList.VERTICAL);

		int level = 0;

		// top label
		c.gridy = level;
		c.gridx = 0;
		c.gridwidth = 2;
		panel.add(betsListLabel, c);

		// list
		level++;
		c.gridy = level;
		c.ipady = 250;
		c.ipadx = 450;
		c.gridwidth = 3;
		panel.add(betsScrollPane, c);
		c.gridwidth = 1;
		c.ipady = 0;
		c.ipadx = 0;

		// id
		level++;
		c.gridy = level;
		c.gridwidth = 1;
		panel.add(idLabel, c);
		c.gridx = 1;
		c.gridwidth = 1;
		panel.add(idField, c);
		c.gridx = 2;
		panel.add(searchBetButton, c);

		// buts
		level++;
		c.gridy = level;
		c.gridx = 0;
		panel.add(editButton, c);
		c.gridx = 1;
		panel.add(rmBetButton, c);
		c.gridx = 2;
		panel.add(cancelButton, c);

		// error
		level++;
		c.gridy = level;
		c.gridx = 0;
		c.gridwidth = 3;
		panel.add(errorLabel, c);

	}
}
