package lazsist.v1.graphics;

import java.awt.ComponentOrientation;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import lazsist.v1.Bet;
import lazsist.v1.LazybSist;
import lazsist.v1.LsEvent;

/**
 * 
 */

/**
 * @author karolis
 * 
 */
@SuppressWarnings("serial")
public class EditBetWindow extends JFrame {
	final static boolean shouldFill = true;
	final static boolean RIGHT_TO_LEFT = false;

	LazybSist laSystem;
	Bet editedBet;

	private JLabel fNameLabel = new JLabel("Vardas:");
	private JLabel lNameLabel = new JLabel("Pavardė:");
	private JLabel eventLabel = new JLabel("Renginys:");
	private JLabel teamLabel = new JLabel("Komanda:");
	private JLabel betLabel = new JLabel("Statymas:");
	private JLabel rateLabel = new JLabel("Koeficientas:");
	private JLabel idLabel = new JLabel("ID:");
	private JLabel errorLabel = new JLabel();

	private JTextField fNameField = new JTextField();
	private JTextField lNameField = new JTextField();
	private JTextField eventField = new JTextField();
	private JTextField teamField = new JTextField();
	private JTextField betField = new JTextField();
	private JTextField rateField = new JTextField();
	private JTextField idField = new JTextField();

	private JButton doneBut = new JButton("Išsaugoti");
	private JButton cancelBut = new JButton("Atšaukti");

	public EditBetWindow(int idx, LazybSist laSystem) {
		this.laSystem = laSystem;
		this.editedBet = this.laSystem.getBetByIdx(idx);

		eventField.setEditable(false);
		teamField.setEditable(false);
		betField.setEditable(false);
		rateField.setEditable(false);
		idField.setEditable(false);

		loadBet();
		initButs();
		initUI();
	}

	private void loadBet() {
		fNameField.setText(editedBet.getFName());
		lNameField.setText(editedBet.getLName());

		int eventIdx = laSystem.getEventIdxById(editedBet.getEventId());
		LsEvent tmp = laSystem.getEventByIdx(eventIdx);
		eventField.setText(tmp.getEventName());
		teamField.setText(tmp.getTeam(editedBet.getTeamIdx()).getName());
		betField.setText(Double.toString(editedBet.getBet()));
		rateField.setText(Double.toString(editedBet.getRate()));
		idField.setText(Integer.toString(editedBet.getBetId()));
	}

	private void saveBet() {
		editedBet.setfName(fNameField.getText());
		editedBet.setlName(lNameField.getText());
	}

	private void initButs() {
		doneBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				saveBet();
				dispose();
			}
		});

		cancelBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
	}

	private void initUI() {
		setTitle("Keisti statymą");
		setSize(300, 290);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		JPanel panel = new JPanel();

		if (RIGHT_TO_LEFT) {
			panel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		}

		panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		panel.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();
		if (shouldFill) {
			// natural height, maximum width
			c.fill = GridBagConstraints.HORIZONTAL;
		}

		int level = 0;

		c.gridy = level;
		c.gridx = 0;
		c.ipadx = 50;
		panel.add(fNameLabel, c);
		c.gridx = 1;
		panel.add(fNameField, c);

		level++;
		c.gridy = level;
		c.gridx = 0;
		panel.add(lNameLabel, c);
		c.gridx = 1;
		panel.add(lNameField, c);

		level++;
		c.gridy = level;
		c.gridx = 0;
		panel.add(betLabel, c);
		c.gridx = 1;
		panel.add(betField, c);

		level++;
		c.gridy = level;
		c.gridx = 0;
		panel.add(eventLabel, c);
		c.gridx = 1;
		panel.add(eventField, c);

		level++;
		c.gridy = level;
		c.gridx = 0;
		panel.add(teamLabel, c);
		c.gridx = 1;
		panel.add(teamField, c);

		level++;
		c.gridy = level;
		c.gridx = 0;
		panel.add(betLabel, c);
		c.gridx = 1;
		panel.add(betField, c);

		level++;
		c.gridy = level;
		c.gridx = 0;
		panel.add(rateLabel, c);
		c.gridx = 1;
		panel.add(rateField, c);

		level++;
		c.gridy = level;
		c.gridx = 0;
		panel.add(idLabel, c);
		c.gridx = 1;
		panel.add(idField, c);

		// Buttons
		level++;
		c.gridy = level;
		c.gridx = 0;
		panel.add(cancelBut, c);
		c.gridx = 1;
		panel.add(doneBut, c);

		// error
		level++;
		c.gridy = level;
		c.gridx = 0;
		c.gridwidth = 2;
		panel.add(errorLabel, c);

		add(panel);
	}
}
