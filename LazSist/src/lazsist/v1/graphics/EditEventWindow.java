package lazsist.v1.graphics;

import java.awt.ComponentOrientation;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import lazsist.v1.LazybSist;
import lazsist.v1.LsEvent;

/**
 * 
 */

/**
 * @author karolis
 * 
 */
@SuppressWarnings("serial")
public class EditEventWindow extends JFrame {
	final static boolean shouldFill = true;
	final static boolean RIGHT_TO_LEFT = false;

	private LsEvent editedEvent;

	private JLabel nameLabel = new JLabel("Renginio pavadinimas");
	private JLabel dateLabel = new JLabel("Renginio data");
	private JLabel teamNameLabel = new JLabel("Komandos pavadinimas:");
	private JLabel teamRatioLabel = new JLabel("Koeficientas:");
	private JLabel teamListLabel = new JLabel("Komandų sąrašas:");
	private JLabel locationLabel = new JLabel("Renginio vieta:");
	private JLabel errorLabel = new JLabel();

	private JTextField nameField = new JTextField();
	private JTextField locationField = new JTextField();
	private JTextField teamNameField = new JTextField();
	private JTextField teamRatioField = new JTextField();

	private DefaultListModel<String> teamListModel = new DefaultListModel<String>();
	private JList<String> teamList = new JList<String>(teamListModel);
	private JScrollPane teamsScrollPane = new JScrollPane(teamList);

	// Date fields
	private JTextField dayField = new JTextField("DD");
	private JTextField monthField = new JTextField("MM");
	private JTextField yearField = new JTextField("YYYY");

	private JButton addTeamBut = new JButton("Pridėti komandą");
	private JButton rmTeamBut = new JButton("Pašalinti komandą");
	private JButton doneBut = new JButton("Išsaugoti ");
	private JButton cancelBut = new JButton("Atšaukti");

	// Date formats
	private final SimpleDateFormat sdfDay = new SimpleDateFormat("dd");
	private final SimpleDateFormat sdfMonth = new SimpleDateFormat("MM");
	private final SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

	public EditEventWindow(int idx, LazybSist laSystem) {
		editedEvent = laSystem.getEventByIdx(idx);

		loadEvent();
		initButs();
		initUI();
	}

	public EditEventWindow(LsEvent event) {
		editedEvent = event;

		loadEvent();
		initButs();
		initUI();
	}

	private void initLists() {
		teamListModel.clear();
		for (int i = 0; i < editedEvent.getTeamCount(); i++) {
			String tmp = editedEvent.getTeam(i).getName() + " "
					+ editedEvent.getTeam(i).getRate();
			teamListModel.addElement(tmp);
		}
	}

	private void loadEvent() {
		nameField.setText(editedEvent.getEventName());
		locationField.setText(editedEvent.getEventLocation());

		dayField.setText(sdfDay.format(editedEvent.getEventDate()));
		monthField.setText(sdfMonth.format(editedEvent.getEventDate()));
		yearField.setText(sdfYear.format(editedEvent.getEventDate()));

		initLists();
	}

	private void initButs() {
		// TODO Auto-generated method stub

		addTeamBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				errorLabel.setText("");
				String name = teamNameField.getText();
				Double d;

				if (name.isEmpty()) {
					errorLabel.setText("Nėra komandos vardo");
					return;
				}

				try {
					d = Double.valueOf(teamRatioField.getText());
				} catch (NumberFormatException nfe) {
					errorLabel.setText("blogas koeficiento formatas");
					return;
				}

				if (d > 3 || d < 1) {
					errorLabel.setText("blogas koeficiento formatas");
					return;
				}

				editedEvent.addTeam(name, d);// TODO sepparate lists
				name = name + ", " + d;
				teamListModel.addElement(name);

				teamNameField.setText("");
				teamRatioField.setText("");

				initLists();
				// newEvent.printTeams();
			}
		});

		rmTeamBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				errorLabel.setText("");
				int index = teamList.getSelectedIndex();
				if (index == -1) {
					errorLabel.setText("Nepasirinkta komanda");
					return;
				}
				teamListModel.removeElementAt(index);
				editedEvent.rmTeamAtIdx(index);
			}
		});

		doneBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				errorLabel.setText("");

				if (nameField.getText().equals("")
						|| locationField.getText().equals("")) {
					errorLabel.setText("Klaida pavadinime/vietovėje");
					return;
				}

				if (editedEvent.getTeamCount() < 2) {
					errorLabel.setText("Nepridėtos komandos ");
					System.out.println(editedEvent.getTeamCount());
					return;
				}

				editedEvent.setEventName(nameField.getText());
				editedEvent.setEventLocation(locationField.getText());

				Date tmpDate = new Date();
				try {
					tmpDate = sdf.parse(getDateString());
				} catch (ParseException e1) {
					e1.printStackTrace();
					errorLabel.setText("blogas datos formatas");
				}
				editedEvent.setEventDate(tmpDate);

				System.out.println(editedEvent.getEventName());

				dispose();
			}
		});

		cancelBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO separate teams lists
				dispose();
			}
		});
	}

	private String getDateString() {
		String tmp = new String();
		tmp = yearField.getText() + monthField.getText() + dayField.getText();// yyyyMMdd
		// System.out.println(tmp);
		return tmp;
	}

	private void initUI() {
		// TODO Auto-generated method stub
		setTitle("Redaguoti renginį");
		setSize(350, 350);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		JPanel panel = new JPanel();

		if (RIGHT_TO_LEFT) {
			panel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		}

		panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		panel.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();
		if (shouldFill) {
			// natural height, maximum width
			c.fill = GridBagConstraints.HORIZONTAL;
		}

		teamList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		teamList.setLayoutOrientation(JList.VERTICAL);

		int level = 0;

		c.gridx = 0;
		c.gridy = level;
		panel.add(nameLabel, c);
		c.gridx = 1;
		c.gridwidth = 3;
		panel.add(nameField, c);
		c.gridwidth = 1;

		level++;
		// DATE
		c.gridx = 0;
		c.gridy = level;
		panel.add(dateLabel, c);
		c.gridx = 1;
		// c.gridy = 1;
		c.ipadx = 45;
		panel.add(yearField, c);
		c.gridx = 2;
		// c.gridy = 1;
		panel.add(monthField, c);
		c.gridx = 3;
		// c.gridy = 1;
		panel.add(dayField, c);
		c.ipadx = 0;

		level++;
		// TEAMS
		c.gridx = 0;
		c.gridy = level;
		panel.add(teamNameLabel, c);
		c.gridx = 1;
		// c.gridy = 2;
		c.gridwidth = 3;
		panel.add(teamNameField, c);
		c.gridwidth = 1;
		// nl
		level++;
		c.gridx = 0;
		c.gridy = level;
		panel.add(teamRatioLabel, c);
		c.gridx = 1;
		// c.gridy = 3;
		c.gridwidth = 3;
		panel.add(teamRatioField, c);
		// c.gridwidth = 1;
		level++;
		c.gridx = 1;
		c.gridy = level;
		c.gridwidth = 3;
		panel.add(addTeamBut, c);
		c.gridwidth = 1;

		level++;
		// TEAM LIST
		c.ipady = 50;
		c.gridx = 0;
		c.gridy = level;
		panel.add(teamListLabel, c);
		c.gridx = 1;
		// c.gridy = 5;
		c.gridwidth = 3;
		panel.add(teamsScrollPane, c);
		c.ipady = 0;
		c.gridwidth = 1;

		level++;
		// RM TEAM but
		c.gridy = level;
		c.gridx = 1;
		c.gridwidth = 3;
		panel.add(rmTeamBut, c);
		c.gridwidth = 1;

		level++;
		// LOCATION
		c.gridx = 0;
		c.gridy = level;
		panel.add(locationLabel, c);
		c.gridx = 1;
		// c.gridy = 7;
		c.gridwidth = 3;
		panel.add(locationField, c);
		c.gridwidth = 1;

		level++;
		// BUTTONS
		c.gridx = 0;
		c.gridy = level;
		panel.add(cancelBut, c);
		c.gridx = 1;
		// c.gridy = 8;
		c.gridwidth = 3;
		panel.add(doneBut, c);
		// c.gridwidth = 1;

		level++;
		c.gridx = 0;
		c.gridy = level;
		c.gridwidth = 4;
		// c.ipady = 20;
		panel.add(errorLabel, c);
		// c.gridwidth = 1;
		// c.ipady = 0;

		add(panel);
	}
}
