package lazsist.v1.graphics;

import java.awt.ComponentOrientation;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import lazsist.BetException;
import lazsist.v1.LazybSist;
import lazsist.v1.LsEvent;

@SuppressWarnings("serial")
public class EventsListWindow extends JFrame {
	protected final static boolean shouldFill = true;
	protected final static boolean RIGHT_TO_LEFT = false;

	protected LazybSist laSystem;

	protected JLabel eventsListLabel = new JLabel("Pasirinkite renginį");
	protected JLabel errorLabel = new JLabel();

	private DefaultListModel<String> eventsListModel = new DefaultListModel<String>();
	protected JList<String> eventsList = new JList<String>(eventsListModel);
	protected JScrollPane eventsScrollPane = new JScrollPane(eventsList);

	protected JButton editButton = new JButton("Redaguoti");
	protected JButton cancelButton = new JButton("Atšaukti");
	protected JButton rmEventButton = new JButton("Naikinti renginį");
	protected JButton setWinTeamButton = new JButton("Nustatyti laimėtojus");
	protected JButton getWinnersButton = new JButton("Laimėtojų sąrašas");

	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");

	public EventsListWindow(LazybSist laSystem) {
		this.laSystem = laSystem;

		initList();
		initButs();
		initUI();
	}

	private String constructLine(LsEvent event) {
		String newLine = sdf.format(event.getEventDate()) + ", "
				+ event.getEventName() + ", ID: " + event.getEventId();
		if (event.getWinnerIsSet()) {
			newLine = newLine + ", Laimėjo: "
					+ event.getTeam(event.getWinnerIdx()).getName();
		}
		return newLine;
	}

	private void initList() {
		String tmp;
		eventsListModel.clear();
		for (int i = 0; i < laSystem.getEventsList().size(); i++) {
			tmp = constructLine(laSystem.getEventsList().get(i));
			eventsListModel.addElement(tmp);
		}
	}

	protected void initButs() {
		cancelButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});

		editButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				errorLabel.setText("");
				int idx = eventsList.getSelectedIndex();
				if (idx < 0) {
					errorLabel.setText("Nepasirinktas ivykis");
					return;
				}
				if (laSystem.getEventByIdx(idx).getBetCount() != 0
						|| laSystem.getEventByIdx(idx).getWinnerIsSet()) {
					errorLabel.setText("Negalima redaguoti įvykio");
					System.out.println("bet count: "
							+ laSystem.getEventByIdx(idx).getBetCount());
				} else {
					EditEventWindow editEventWindow = new EditEventWindow(idx,
							laSystem);
					editEventWindow.setVisible(true);
					dispose();
				}
			}
		});

		rmEventButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				errorLabel.setText("");
				int rmIdx = eventsList.getSelectedIndex();

				// If there's no bets - rmEvent
				if (laSystem.getEventByIdx(rmIdx).getBetCount() == 0) {
					try {
						laSystem.rmEvent(rmIdx);
					} catch (BetException e1) {
						e1.printStackTrace();
					}
					initList();
				} else {
					errorLabel.setText("Negalima ištrinti įvykio");
					System.out.println("bet count: "
							+ laSystem.getEventByIdx(rmIdx).getBetCount());
				}
			}
		});

		setWinTeamButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				errorLabel.setText("");
				int idx = eventsList.getSelectedIndex();
				if (idx < 0) {
					errorLabel.setText("Nepasirinktas ivykis");
					return;
				}

				SetWinnerWindow sww = new SetWinnerWindow(laSystem, idx);
				sww.setVisible(true);

				dispose();
			}
		});

		getWinnersButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				errorLabel.setText("");
				int idx = eventsList.getSelectedIndex();
				// TODO checks
				if (idx < 0) {
					errorLabel.setText("Nepasirinktas ivykis");
					return;
				}

				GetWinnersWindow gww = new GetWinnersWindow(laSystem, idx);
				gww.setVisible(true);
			}
		});
	}

	protected void initUI() {
		setTitle("Renginių sąrašas");
		setSize(400, 300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		JPanel panel = new JPanel();

		if (RIGHT_TO_LEFT) {
			panel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		}

		panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		panel.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();
		if (shouldFill) {
			// natural height, maximum width
			c.fill = GridBagConstraints.HORIZONTAL;
		}

		eventsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		eventsList.setLayoutOrientation(JList.VERTICAL);

		int level = 0;

		c.gridy = level;
		c.gridx = 0;
		c.gridwidth = 3;
		panel.add(eventsListLabel, c);

		level++;
		c.gridy = level;
		c.gridx = 0;
		c.ipady = 150;
		panel.add(eventsScrollPane, c);

		level++;
		c.gridy = level;
		c.gridwidth = 1;
		c.ipady = 0;
		c.gridx = 1;
		panel.add(rmEventButton, c);
		c.gridx = 2;
		panel.add(editButton, c);

		level++;
		c.gridy = level;
		c.gridx = 0;
		panel.add(setWinTeamButton, c);
		c.gridx = 1;
		panel.add(getWinnersButton, c);
		c.gridx = 2;
		panel.add(cancelButton, c);

		level++;
		c.gridy = level;
		c.gridx = 0;
		c.gridwidth = 3;
		panel.add(errorLabel, c);

		add(panel);
	}
}
