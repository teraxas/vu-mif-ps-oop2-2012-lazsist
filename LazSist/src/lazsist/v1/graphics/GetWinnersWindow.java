package lazsist.v1.graphics;

import java.awt.ComponentOrientation;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import lazsist.v1.Bet;
import lazsist.v1.LazybSist;
import lazsist.v1.LsEvent;

@SuppressWarnings("serial")
public class GetWinnersWindow extends JFrame {
	final static boolean shouldFill = true;
	final static boolean RIGHT_TO_LEFT = false;

	private LsEvent theEvent;

	private ArrayList<Bet> winnersArrayList;

	private DefaultListModel<String> winnersListModel = new DefaultListModel<String>();
	private JList<String> winnersList = new JList<String>(winnersListModel);
	private JScrollPane winnersScrollPane = new JScrollPane(winnersList);

	private JButton doneBut = new JButton("OK");

	private JLabel winnersLabel = new JLabel("Laimėtojai");

	public GetWinnersWindow(LazybSist laSystem, int eventIdx) {
		this.theEvent = laSystem.getEventByIdx(eventIdx);

		int eventId = laSystem.getEventByIdx(eventIdx).getEventId();
		this.winnersArrayList = laSystem.getEventWinners(eventId);

		initButs();
		initList();
		initUI();
	}

	private void initList() {
		String tmp;
		for (int i = 0; i < winnersArrayList.size(); i++) {
			tmp = winnersArrayList.get(i).toString();
			winnersListModel.addElement(tmp);
		}
	}

	private void initButs() {
		doneBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});

	}

	private void initUI() {
		setTitle(theEvent.getEventName());
		setSize(500, 450);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		JPanel panel = new JPanel();

		if (RIGHT_TO_LEFT) {
			panel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		}

		panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		panel.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();
		if (shouldFill) {
			// natural height, maximum width
			c.fill = GridBagConstraints.HORIZONTAL;
		}

		winnersList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		winnersList.setLayoutOrientation(JList.VERTICAL);

		int level = 0;

		c.gridx = 0;
		c.gridy = level;
		c.gridwidth = 2;
		panel.add(winnersLabel, c);

		level++;
		c.gridy = level;
		c.ipady = 350;
		c.ipadx = 420;
		panel.add(winnersScrollPane, c);
		c.ipady = 0;
		c.ipadx = 0;

		level++;
		c.gridy = level;
		panel.add(doneBut, c);

		add(panel);
	}

}
