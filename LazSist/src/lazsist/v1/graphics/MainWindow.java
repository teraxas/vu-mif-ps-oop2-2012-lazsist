package lazsist.v1.graphics;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import lazsist.v1.LazybSist;
import lazsist.v3.LazybSistV3;

@SuppressWarnings("serial")
public class MainWindow extends JFrame {

	/**
	 * The betting system
	 */
	protected LazybSist laSystem;

	protected JPanel panel = new JPanel();

	// TODO change init :)
	protected JButton newBetBut = new JButton("Naujas statymas");
	protected JButton editBetBut = new JButton("Statymai");
	protected JButton newEventBut = new JButton("Naujas įvykis");
	protected JButton editEventBut = new JButton("Renginiai");
	protected JButton quitBut = new JButton("Baigti darbą");

	/**
	 * Constructor
	 * 
	 * @param laSystem
	 */
	public MainWindow(LazybSist laSystem) {
		initLookAndFeel();
		this.setLaSystem(laSystem);

		initButs();
		initUI();
	}

	public LazybSist getLaSystem() {
		return laSystem;
	}

	public void setLaSystem(LazybSist laSystem) {
		this.laSystem = laSystem;
	}

	/**
	 * Set system look and feel
	 */
	private void initLookAndFeel() {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (InstantiationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (UnsupportedLookAndFeelException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	/**
	 * Initialize buttons
	 */
	protected void initButs() {
		newBetBut = new JButton("Naujas statymas");
		editBetBut = new JButton("Statymai");
		newEventBut = new JButton("Naujas įvykis");
		editEventBut = new JButton("Renginiai");
		quitBut = new JButton("Baigti darbą");

		newBetBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				NewBetWindow newBetWindow = new NewBetWindow(getLaSystem());
				newBetWindow.setVisible(true);
			}
		});

		newEventBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				NewEventWindow newEventWindow = new NewEventWindow(
						getLaSystem());
				newEventWindow.setVisible(true);

			}
		});

		quitBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(getDefaultCloseOperation());
			}
		});

		if ((laSystem instanceof LazybSistV3) == false) {
			editEventBut.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					EventsListWindow eventsListWindow = new EventsListWindow(
							getLaSystem());
					eventsListWindow.setVisible(true);
				}
			});

			editBetBut.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					// TODO Auto-generated method stub
					BetsListWindow betsListW = new BetsListWindow(getLaSystem());
					betsListW.setVisible(true);
				}
			});
		}
	}

	/**
	 * Initialize UI
	 */
	protected void initUI() {

		setTitle("LazSist");
		setSize(200, 400);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		JPanel panel = new JPanel();

		panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		panel.setLayout(new GridLayout(0, 1, 0, 5));

		// Add components
		panel.add(newBetBut);
		panel.add(editBetBut);
		panel.add(newEventBut);
		panel.add(editEventBut);
		panel.add(quitBut);

		add(panel);
	}
}
