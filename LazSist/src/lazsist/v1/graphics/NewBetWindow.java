package lazsist.v1.graphics;

import java.awt.ComponentOrientation;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import lazsist.v1.Bet;
import lazsist.v1.LazybSist;
import lazsist.v1.LsEvent;

@SuppressWarnings("serial")
public class NewBetWindow extends JFrame {
	final static boolean shouldFill = true;
	final static boolean RIGHT_TO_LEFT = false;

	LazybSist laSystem;

	ArrayList<LsEvent> eventList;

	private JLabel fNameLabel = new JLabel("Vardas:");
	private JLabel lNameLabel = new JLabel("Pavardė:");
	private JLabel eventsListLabel = new JLabel("Renginiai:");
	private JLabel teamsListLabel = new JLabel("Komandos:");
	private JLabel betLabel = new JLabel("Statymas:");
	private JLabel errorLabel = new JLabel();

	private JButton doneBut = new JButton("Išsaugoti");
	private JButton cancelBut = new JButton("Atšaukti");

	private JTextField fNameField = new JTextField();
	private JTextField lNameField = new JTextField();
	private JTextField betField = new JTextField();

	private DefaultListModel<String> eventsListModel = new DefaultListModel<String>();
	private JList<String> eventsList = new JList<String>(eventsListModel);
	private JScrollPane eventsScrollPane = new JScrollPane(eventsList);

	private DefaultListModel<String> teamListModel = new DefaultListModel<String>();
	private JList<String> teamList = new JList<String>(teamListModel);
	private JScrollPane teamsScrollPane = new JScrollPane(teamList);

	private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

	public NewBetWindow(LazybSist laSystem) {
		this.laSystem = laSystem;

		eventList = laSystem.getEventsList();

		initButs();
		initLists();
		initUI();
	}

	public Bet formBet(int team) {
		Double betSum;
		try {
			betSum = Double.parseDouble(betField.getText());
		} catch (NumberFormatException nfe) {
			errorLabel.setText("blogas koeficiento formatas");
			return null;
		}
		Bet newBet = new Bet(fNameField.getText(), lNameField.getText(),
				eventList.get(eventsList.getSelectedIndex()).getEventId(),
				eventList.get(eventsList.getSelectedIndex()).getTeam(team)
						.getRate(), betSum, team);
		return newBet;
	}

	private void initButs() {
		doneBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				errorLabel.setText("");

				if (fNameField.getText() == "" || lNameField.getText() == ""
						|| betField.getText() == "") {
					errorLabel.setText("Trūksta informacijos");
					return;
				}

				int team = teamList.getSelectedIndex();

				if (team == -1) {
					errorLabel.setText("Nepasirinkta komanda");
					return;
				}

				Bet formedBet = formBet(team);
				if (formedBet != null) {
					laSystem.addBet(formBet(team));
					dispose();
				}
			}
		});

		cancelBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}

	private void initLists() {
		// init events list
		for (int i = 0; i < eventList.size(); i++) {
			String tmp = eventList.get(i).getEventName() + ", "
					+ sdf.format(eventList.get(i).getEventDate());
			eventsListModel.addElement(tmp);
		}

		eventsList.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				LsEvent tmp = eventList.get(eventsList.getSelectedIndex());
				String t1 = "Team count:"
						+ eventList.get(eventsList.getSelectedIndex())
								.getTeamCount();
				System.out.println(t1);

				teamListModel.clear();
				for (int i = 0; i < tmp.getTeamCount(); i++) {
					String tmpS = tmp.getTeam(i).getName() + " "
							+ tmp.getTeam(i).getRate();
					teamListModel.addElement(tmpS);
				}
			}
		});
	}

	private void initUI() {
		setTitle("Naujas Statymas");
		setSize(350, 350);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		JPanel panel = new JPanel();

		if (RIGHT_TO_LEFT) {
			panel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		}

		panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		panel.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();
		if (shouldFill) {
			// natural height, maximum width
			c.fill = GridBagConstraints.HORIZONTAL;
		}

		teamList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		teamList.setLayoutOrientation(JList.VERTICAL);

		eventsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		eventsList.setLayoutOrientation(JList.VERTICAL);

		int level = 0;

		c.gridy = level;
		c.gridx = 0;
		panel.add(fNameLabel, c);
		c.gridx = 1;
		panel.add(fNameField, c);

		level++;
		c.gridy = level;
		c.gridx = 0;
		panel.add(lNameLabel, c);
		c.gridx = 1;
		panel.add(lNameField, c);

		level++;
		c.gridy = level;
		c.gridx = 0;
		panel.add(eventsListLabel, c);
		c.gridx = 1;
		panel.add(teamsListLabel, c);

		level++;
		c.gridy = level;
		c.gridx = 0;
		c.ipadx = 155;
		c.ipady = 150;
		panel.add(eventsScrollPane, c);
		c.gridx = 1;
		panel.add(teamsScrollPane, c);
		c.ipadx = 0;
		c.ipady = 0;

		level++;
		c.gridy = level;
		c.gridx = 0;
		panel.add(betLabel, c);
		c.gridx = 1;
		panel.add(betField, c);

		level++;
		c.gridy = level;
		c.gridx = 0;
		panel.add(cancelBut, c);
		c.gridx = 1;
		panel.add(doneBut, c);

		level++;
		c.gridy = level;
		c.gridx = 0;
		c.gridwidth = 2;
		panel.add(errorLabel, c);

		add(panel);
	}
}
