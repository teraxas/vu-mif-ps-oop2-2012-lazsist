package lazsist.v1.graphics;

import java.awt.ComponentOrientation;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import lazsist.v1.LazybSist;
import lazsist.v1.LsEvent;

@SuppressWarnings("serial")
public class SetWinnerWindow extends JFrame {
	final static boolean shouldFill = true;
	final static boolean RIGHT_TO_LEFT = false;

	private LazybSist laSystem;
	private LsEvent editedEvent;

	private DefaultListModel<String> teamListModel = new DefaultListModel<String>();
	private JList<String> teamList = new JList<String>(teamListModel);
	private JScrollPane teamsScrollPane = new JScrollPane(teamList);

	private JLabel teamListLabel = new JLabel("Komandų sąrašas:");
	private JLabel errorLabel = new JLabel();

	private JButton doneBut = new JButton("Išsaugoti ");
	private JButton cancelBut = new JButton("Atšaukti");

	public SetWinnerWindow(LazybSist laSystem, int eventIdx) {
		// TODO Auto-generated constructor stub
		this.laSystem = laSystem;
		this.editedEvent = laSystem.getEventByIdx(eventIdx);

		initLists();
		initButs();
		initUI();
	}

	private void initLists() {
		teamListModel.clear();
		for (int i = 0; i < editedEvent.getTeamCount(); i++) {
			String tmp = editedEvent.getTeam(i).getName() + " "
					+ editedEvent.getTeam(i).getRate();
			teamListModel.addElement(tmp);
		}
	}

	private void initButs() {
		// TODO
		doneBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				int team = teamList.getSelectedIndex();

				if (team == -1) {
					errorLabel.setText("Nepasirinkta komanda");
					return;
				}

				if (editedEvent.getWinnerIsSet()) {
					errorLabel.setText("Laimėtojas jau nustatytas");
					return;
				}

				editedEvent.setWinner(team);
				laSystem.setEventWinners(editedEvent.getEventId());

				dispose();
			}
		});

		cancelBut.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}

	private void initUI() {
		setTitle(editedEvent.getEventName());
		setSize(180, 250);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		JPanel panel = new JPanel();

		if (RIGHT_TO_LEFT) {
			panel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		}

		panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		panel.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();
		if (shouldFill) {
			// natural height, maximum width
			c.fill = GridBagConstraints.HORIZONTAL;
		}

		teamList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		teamList.setLayoutOrientation(JList.VERTICAL);

		int level = 0;

		c.gridx = 0;
		c.gridy = level;
		c.gridwidth = 2;
		panel.add(teamListLabel, c);
		// c.gridwidth = 1;

		level++;
		c.gridy = level;
		c.ipady = 120;
		c.ipadx = 120;
		panel.add(teamsScrollPane, c);
		c.ipady = 0;
		c.ipadx = 0;
		c.gridwidth = 1;

		level++;
		c.gridy = level;
		panel.add(cancelBut, c);
		c.gridx = 1;
		panel.add(doneBut, c);
		c.gridx = 0;

		level++;
		c.gridwidth = 2;
		c.gridy = level;
		panel.add(errorLabel, c);

		add(panel);
	}
}
