package lazsist.v2;

import java.io.Serializable;

import lazsist.v1.LazybSist;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.DefaultPieDataset;

/**
 * @author karolis
 * 
 */
public class LazybSistV2 extends LazybSist implements Serializable {

	private static final long serialVersionUID = 4433446020250651031L;

	/**
	 * Default constructor
	 */
	public LazybSistV2() {
		super();
	}

	/**
	 * Generate pie chart of bet sizes
	 * 
	 * @return Pie chart
	 */
	public JFreeChart getBetsPieChart() {
		String tmp;
		DefaultPieDataset data = new DefaultPieDataset();
		for (int i = 0; i < getBetsList().size(); i++) {
			tmp = getBetByIdx(i).getFName() + " " + getBetByIdx(i).getLName();
			data.setValue(tmp, getBetByIdx(i).getBet());
		}

		JFreeChart chart = ChartFactory.createPieChart("Statymai", data, true,
				false, false);

		return chart;
	}

	/**
	 * Generates bar chart of bets per event
	 * 
	 * @return Bar chart
	 */
	public JFreeChart getEventsBarChart() {
		DefaultCategoryDataset data = new DefaultCategoryDataset();
		for (int i = 0; i < getEventsList().size(); i++) {
			data.setValue(getEventByIdx(i).getBetCount(), "Kiekis",
					getEventByIdx(i).getEventName());
		}

		JFreeChart chart = ChartFactory.createBarChart("Renginiai",
				"Renginiai", // Category Axis
				"Statymai", // Value Axis
				data, PlotOrientation.VERTICAL,// Plot orientation
				true, false, false);

		return chart;
	}

	/**
	 * Loads test data of LazybSist. Collection tests are commented.
	 */
	@Override
	public void test() {
		super.test();

		// Collection TEST
		// MapSetTest bct = new MapSetTest(this);
		// bct.printBetsAllEvents();
	}
}
