package lazsist.v2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import lazsist.v1.Bet;
import lazsist.v1.LsEvent;

public class MapSetTest implements Serializable {

	private static final long serialVersionUID = 7962246832074291651L;

	private Set<LsEvent> eventsSet;
	private Map<Integer, ArrayList<Bet>> betsMap;

	private ArrayList<LsEvent> eventsList;
	private ArrayList<Bet> betsList;

	public MapSetTest(LazybSistV2 laSystem) {
		this.eventsList = laSystem.getEventsList();
		this.betsList = laSystem.getBetsList();

		loadBetsMap();
		loadEventsSet();
	}

	public void printBetsAllEvents() {
		java.util.Iterator<LsEvent> it = eventsSet.iterator();

		while (it.hasNext()) {
			LsEvent tmp = it.next();
			System.out.println("   " + tmp.toString() + ":");
			ArrayList<Bet> tmpBets = betsMap.get(tmp.getEventId());
			for (Bet bet : tmpBets) {
				System.out.println(bet);
			}
		}
	}

	private void loadBetsMap() {
		betsMap = new HashMap<Integer, ArrayList<Bet>>();
		// Bet bet;
		for (Bet bet : betsList) {
			if (betsMap.containsKey(bet.getEventId())) {
				betsMap.get(bet.getEventId()).add(bet);
			} else {
				ArrayList<Bet> tmpList = new ArrayList<Bet>();
				tmpList.add(bet);
				betsMap.put(bet.getEventId(), tmpList);
			}
		}
	}

	private void loadEventsSet() {
		eventsSet = new HashSet<LsEvent>();

		for (LsEvent event : eventsList) {
			eventsSet.add(event);
		}
	}

	public void printEvents() {
		java.util.Iterator<LsEvent> it = eventsSet.iterator();

		while (it.hasNext()) {
			System.out.println(it.next().toString());
		}
	}

	public ArrayList<Bet> getBetsForEvent(LsEvent e) {
		if (betsMap.containsKey(e.getEventId())) {
			return betsMap.get(e);
		}
		return null;
	}

	public Set<LsEvent> getEventsSet() {
		return eventsSet;
	}

	public Map<Integer, ArrayList<Bet>> getBetsMap() {
		return betsMap;
	}
}
