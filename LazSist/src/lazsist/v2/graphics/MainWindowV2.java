package lazsist.v2.graphics;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

import lazsist.v1.LazybSist;
import lazsist.v1.graphics.MainWindow;
import lazsist.v2.LazybSistV2;

import org.jfree.chart.ChartFrame;

@SuppressWarnings("serial")
public class MainWindowV2 extends MainWindow {

	private LazybSistV2 laSistV2;

	protected JButton statsBut;

	public MainWindowV2(LazybSist laSystem) {
		super(laSystem);

		if (laSystem instanceof LazybSistV2) {
			laSistV2 = (LazybSistV2) laSystem;
		}
	}

	// show BOTH charts
	private void showAllCharts() {
		ChartFrame frame = new ChartFrame("Statistika",
				laSistV2.getBetsPieChart());
		frame.setSize(500, 500);
		frame.setLocationRelativeTo(null);

		ChartFrame frame2 = new ChartFrame("Statistika",
				laSistV2.getEventsBarChart());
		frame2.setSize(500, 500);
		frame2.setLocationRelativeTo(null);

		frame.setVisible(true);
		frame2.setVisible(true);
	}

	@Override
	protected void initButs() {
		super.initButs();
		if (laSystem instanceof LazybSistV2) {
			statsBut = new JButton("Statistika");
			statsBut.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					if (laSystem.testMode)
						System.out.println("Show CHARTS");
					showAllCharts();
				}
			});

			if (laSystem.testMode)
				System.out.println("Stats button initialised");
		}
	}

	@Override
	protected void initUI() {
		setTitle("LazSist");
		setSize(200, 400);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		JPanel panel = new JPanel();

		panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		panel.setLayout(new GridLayout(0, 1, 0, 5));

		// Add components
		panel.add(newBetBut);
		panel.add(editBetBut);
		panel.add(newEventBut);
		panel.add(editEventBut);
		if (laSystem instanceof LazybSistV2) {
			panel.add(statsBut);
		}
		panel.add(quitBut);

		add(panel);
	}
}
