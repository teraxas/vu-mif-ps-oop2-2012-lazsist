package lazsist.v3;

import java.io.Serializable;
import java.util.Comparator;

import lazsist.v1.Bet;

public class BetSizesComparator implements Comparator<Bet>, Serializable {

	private static final long serialVersionUID = -8680411800335283365L;

	@Override
	public int compare(Bet arg0, Bet arg1) {
		if (arg0.getBet() > arg1.getBet())
			return -1;
		if (arg0.getBet() < arg1.getBet())
			return 1;
		return 0;
	}

}
