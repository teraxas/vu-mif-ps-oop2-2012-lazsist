package lazsist.v3;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import lazsist.v1.Bet;

@XmlRootElement(name = "Bets")
public class Bets implements Serializable {

	private static final long serialVersionUID = 6967309007810994090L;

	ArrayList<Bet> bet;

	public Bets() {

	}

	public ArrayList<Bet> getBets() {
		return bet;
	}

	@javax.xml.bind.annotation.XmlElement(name = "Bet")
	public void setBets(ArrayList<Bet> bet) {
		this.bet = bet;
	}

}
