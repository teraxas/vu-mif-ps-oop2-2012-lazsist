package lazsist.v3;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lazsist.v1.LsEvent;

@XmlRootElement(name = "eventsList")
public class Events implements Serializable {

	private static final long serialVersionUID = -5420934394164925255L;

	private ArrayList<LsEvent> eventsList1;

	public Events() {

	}

	public ArrayList<LsEvent> getEventsList1() {
		return eventsList1;
	}

	@XmlElement(name = "LsEvent")
	public void setEventsList1(ArrayList<LsEvent> eventsList) {
		this.eventsList1 = eventsList;
	}

}
