package lazsist.v3;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.concurrent.LinkedBlockingQueue;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import lazsist.IllegalArgumentException;
import lazsist.v1.Bet;
import lazsist.v1.LsEvent;
import lazsist.v2.LazybSistV2;
import lazsist.v3.threads.Consumer;
import lazsist.v3.threads.Producer;

/**
 * @author karolis
 * 
 */
public class LazybSistV3 extends LazybSistV2 implements Serializable {

	private static final long serialVersionUID = 8287235922220178264L;

	/**
	 * Home directory
	 */
	final File homedir = new File(System.getProperty("user.home"));

	/**
	 * Events xml file filename
	 */
	final String filenameEvents = "laSistEvents.xml";

	/**
	 * Bets xml file filename
	 */
	final String filenameBets = "laSistBets.xml";

	/**
	 * Toggle sorting by bets/name
	 */
	public boolean sortBetsByBets = false;

	/**
	 * Default constructor (LazybSistV2())
	 */
	public LazybSistV3() {
		super();

	}

	/**
	 * Load events from xml file
	 */
	public void loadEventsFromFile() {
		try {
			// File file = new File(homedir, filenameEvents);
			File file = new File(filenameEvents);
			JAXBContext jaxbContext = JAXBContext.newInstance(Events.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			Events evs = (Events) jaxbUnmarshaller.unmarshal(file);

			for (LsEvent e : evs.getEventsList1()) {
				addEvent(e);
				if (this.testMode) {
					System.out.println("Event from file <" + filenameEvents
							+ "> added:");
					System.out.println("   " + e.toString());
					System.out.println("   Teams: " + e.getTeams().toString());
				}
			}

		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Load bets from xml file
	 */
	public void loadBetsFromFile() {
		try {
			// File file = new File(homedir, filenameBets);
			File file = new File(filenameBets);

			JAXBContext jaxbContext = JAXBContext.newInstance(Bets.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			Bets bets1 = (Bets) jaxbUnmarshaller.unmarshal(file);

			for (Bet b : bets1.getBets()) {
				this.addBet(b);
				System.out.println("Bet from file <" + filenameBets
						+ "> added:");
				System.out.println(b.toString());
			}

		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Multi-threaded bets loading
	 */
	public void loadBetsMultiThread() {

		LinkedBlockingQueue<Bet> queue = new LinkedBlockingQueue<Bet>();
		Producer prod = new Producer(queue);
		Consumer cons = new Consumer(queue, this);

		prod.start();
		cons.start();
	}

	/**
	 * Sorts bets ArrayList by bets size
	 */
	public void sortBetsByBetSize() {
		Collections.sort(this.bets, new BetSizesComparator());
	}

	/**
	 * Add event and sort
	 */
	@Override
	public void addEvent(Date date, String name, String loc)
			throws IllegalArgumentException {
		super.addEvent(date, name, loc);
		sortEvents();
	}

	/**
	 * Add event and sort
	 */
	@Override
	public void addEvent(LsEvent e) {
		super.addEvent(e);
		sortEvents();
	}

	/**
	 * Add bet and sort
	 */
	@Override
	public void addBet(Bet b) {
		super.addBet(b);
		sortBets();
	}

	/**
	 * Add bet and sort
	 */
	@Override
	public void addBet(String fName, String lName, double rate, double bet,
			int teamIdx, int eventId) throws IllegalArgumentException {
		super.addBet(fName, lName, rate, bet, teamIdx, eventId);
		sortBets();
	}

	/**
	 * Sort and get events ArrayList
	 */
	@Override
	public ArrayList<LsEvent> getEventsList() {
		sortEvents();
		return super.getEventsList();
	}

	/**
	 * Sort and get bets ArrayList
	 */
	@Override
	public ArrayList<Bet> getBetsList() {
		sortBets();
		return super.getBetsList();
	}

	/**
	 * Sort events
	 */
	private void sortEvents() {
		Collections.sort(events);
		if (this.testMode)
			System.out.println("Events list sorted");
	}

	/**
	 * Sort bets
	 */
	private void sortBets() {
		if (sortBetsByBets)
			sortBetsByBetSize();
		else
			Collections.sort(bets);
		if (this.testMode)
			System.out.println("Bets list sorted");
	}
}
