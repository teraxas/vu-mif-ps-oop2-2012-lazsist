package lazsist.v3;

import java.io.Serializable;
import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;

import lazsist.v1.Team;

import com.sun.xml.txw2.annotation.XmlElement;

@XmlRootElement(name = "teamsLs")
public class Teams implements Serializable {

	private static final long serialVersionUID = 7269232719895187504L;

	private ArrayList<Team> team;

	public Teams() {

	}

	public ArrayList<Team> getTeam() {
		return team;
	}

	@XmlElement
	public void setTeam(ArrayList<Team> team) {
		this.team = team;
	}

}
