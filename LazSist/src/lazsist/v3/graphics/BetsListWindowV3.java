package lazsist.v3.graphics;

import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import lazsist.v1.LazybSist;
import lazsist.v1.graphics.BetsListWindow;
import lazsist.v3.LazybSistV3;

@SuppressWarnings("serial")
public class BetsListWindowV3 extends BetsListWindow {

	final static boolean shouldFill = true;

	private JButton sortByButton;
	private boolean sortBy = true;

	public BetsListWindowV3(LazybSist laSystem) {
		super(laSystem);
	}

	@Override
	protected void initButs() {
		super.initButs();

		if (laSystem instanceof LazybSistV3) {
			sortByButton = new JButton("Rikiuoti pagal sumą");
			sortByButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					if (sortBy) {
						((LazybSistV3) laSystem).sortBetsByBets = true;
						sortByButton.setText("Rikiuot pagal vardą");
						System.out.println("By BET");
						sortBy = false;
					} else {
						((LazybSistV3) laSystem).sortBetsByBets = false;
						sortByButton.setText("Rikiuoti pagal sumą");
						System.out.println("By NAME");
						sortBy = true;
					}
					initList();
				}
			});
			if (laSystem.testMode)
				System.out.println("Sort button initialised");
		}
	}

	@Override
	protected void initUI() {
		super.initUI();

		if (laSystem instanceof LazybSistV3) {
			GridBagConstraints c = new GridBagConstraints();
			if (shouldFill) {
				// natural height, maximum width
				c.fill = GridBagConstraints.HORIZONTAL;
			}

			c.gridx = 2;
			c.gridy = 0;
			panel.add(sortByButton, c);
			if (laSystem.testMode)
				System.out.println("Sort button added");
		}

	}
}
