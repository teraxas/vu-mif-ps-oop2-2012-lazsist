package lazsist.v3.graphics;

import java.awt.ComponentOrientation;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;

import lazsist.v1.LazybSist;
import lazsist.v1.LsEvent;
import lazsist.v1.graphics.EditEventWindow;
import lazsist.v1.graphics.EventsListWindow;
import lazsist.v3.LazybSistV3;

@SuppressWarnings("serial")
public class EventsListWindowV3 extends EventsListWindow {

	private JButton cloneEventButton;

	public EventsListWindowV3(LazybSist laSystem) {
		super(laSystem);
		if (laSystem.testMode)
			System.out.println("Events List V3 opened");
	}

	@Override
	protected void initButs() {
		super.initButs();

		if (laSystem instanceof LazybSistV3) {
			cloneEventButton = new JButton("Klonuoti renginį");
			cloneEventButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					errorLabel.setText("");
					int idx = eventsList.getSelectedIndex();
					if (idx < 0) {
						errorLabel.setText("Nepasirinktas ivykis");
						return;
					}

					LsEvent tmp;
					try {
						tmp = (LsEvent) laSystem.getEventByIdx(idx).clone();
					} catch (CloneNotSupportedException e) {
						errorLabel.setText("Negalima klonuoti");
						e.printStackTrace();
						return;
					}

					laSystem.addEvent(tmp);

					EditEventWindow editEventWindow = new EditEventWindow(tmp);
					editEventWindow.setVisible(true);
					dispose();
				}
			});
		}
	}

	@Override
	protected void initUI() {
		setTitle("Renginių sąrašas");
		setSize(400, 300);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		JPanel panel = new JPanel();

		if (RIGHT_TO_LEFT) {
			panel.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
		}

		panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		panel.setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();
		if (shouldFill) {
			// natural height, maximum width
			c.fill = GridBagConstraints.HORIZONTAL;
		}

		eventsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		eventsList.setLayoutOrientation(JList.VERTICAL);

		int level = 0;

		c.gridy = level;
		c.gridx = 0;
		c.gridwidth = 3;
		panel.add(eventsListLabel, c);

		level++;
		c.gridy = level;
		c.gridx = 0;
		// c.gridwidth = 3;
		c.ipady = 150;
		panel.add(eventsScrollPane, c);

		level++;
		c.gridy = level;
		c.gridx = 0;
		c.gridwidth = 1;
		c.ipady = 0;
		if (super.laSystem instanceof LazybSistV3) {
			panel.add(cloneEventButton, c);
			if (laSystem.testMode)
				System.out.println("V3: clone button added");
		}
		c.gridx = 1;
		panel.add(rmEventButton, c);
		c.gridx = 2;
		panel.add(editButton, c);

		level++;
		c.gridy = level;
		c.gridx = 0;
		panel.add(setWinTeamButton, c);
		c.gridx = 1;
		panel.add(getWinnersButton, c);
		c.gridx = 2;
		panel.add(cancelButton, c);

		level++;
		c.gridy = level;
		c.gridx = 0;
		c.gridwidth = 3;
		panel.add(errorLabel, c);

		add(panel);
	}

}
