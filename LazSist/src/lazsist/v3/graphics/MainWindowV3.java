package lazsist.v3.graphics;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

import lazsist.v1.LazybSist;
import lazsist.v2.LazybSistV2;
import lazsist.v2.graphics.MainWindowV2;
import lazsist.v3.LazybSistV3;

@SuppressWarnings("serial")
public class MainWindowV3 extends MainWindowV2 {
	final static String savedData = "laSysData.ser";

	private JButton loadButton;

	public MainWindowV3(final LazybSist laSystem) {
		super(laSystem);

		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if (laSystem.testMode)
					System.out.println("closing");

				try {
					saveToFile((LazybSistV3) laSystem, savedData);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

	}

	private void saveToFile(LazybSistV3 laSystem, String filename)
			throws IOException {
		ObjectOutputStream out = null;
		try {
			out = new ObjectOutputStream(new BufferedOutputStream(
					new FileOutputStream(filename)));

			out.writeObject(laSystem);
			if (laSystem.testMode)
				System.out.println("state saved");
		} finally {
			out.close();
		}
	}

	@Override
	protected void initButs() {
		super.initButs();

		if (laSystem instanceof LazybSistV3) {
			loadButton = new JButton("Užkrauti iš failo");
			loadButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					((LazybSistV3) laSystem).loadEventsFromFile();
					((LazybSistV3) laSystem).loadBetsMultiThread();
				}
			});

			editEventBut.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					EventsListWindowV3 eventsListWindow = new EventsListWindowV3(
							getLaSystem());
					eventsListWindow.setVisible(true);
				}
			});

			editBetBut.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent arg0) {
					BetsListWindowV3 betsListWindow = new BetsListWindowV3(
							getLaSystem());
					betsListWindow.setVisible(true);
				}
			});

			quitBut.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					try {
						saveToFile((LazybSistV3) laSystem, savedData);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
				}
			});

			if (laSystem.testMode)
				System.out.println("V3 buttons initialized");
		}
	}

	@Override
	protected void initUI() {
		setTitle("LazSist");
		setSize(200, 400);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		JPanel panel = new JPanel();

		panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		panel.setLayout(new GridLayout(0, 1, 0, 5));

		// Add components
		panel.add(newBetBut);
		panel.add(editBetBut);
		panel.add(newEventBut);
		panel.add(editEventBut);
		if (laSystem instanceof LazybSistV2) {
			panel.add(statsBut);
		}
		if (laSystem instanceof LazybSistV3) {
			panel.add(loadButton);
		}
		panel.add(quitBut);

		add(panel);
	}

}