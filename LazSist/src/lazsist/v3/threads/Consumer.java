package lazsist.v3.threads;

import java.util.concurrent.LinkedBlockingQueue;

import lazsist.v1.Bet;
import lazsist.v3.LazybSistV3;

public class Consumer extends Thread {

	LinkedBlockingQueue<Bet> queue;

	LazybSistV3 laSystem;

	public Consumer(LinkedBlockingQueue<Bet> queue, LazybSistV3 laSystem) {
		this.queue = queue;
		this.laSystem = laSystem;
	}

	@Override
	public void run() {
		super.run();
		try {
			addBetsToSystem();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void addBetsToSystem() throws InterruptedException {
		sleep(50);
		while (queue.iterator().hasNext()) {
			Bet b = queue.take();
			laSystem.addBet(b);
			System.out.println("Bet ADDED to laSystem: " + b.toString());
			sleep(80);
		}
		return;
	}

}
