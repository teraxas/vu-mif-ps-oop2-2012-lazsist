package lazsist.v3.threads;

import java.io.File;
import java.util.concurrent.LinkedBlockingQueue;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import lazsist.v1.Bet;
import lazsist.v3.Bets;

/**
 * 
 * @author karolis
 *
 */
public class Producer extends Thread {

	final File homedir = new File(System.getProperty("user.home"));
	final String filenameBets = "laSistBets.xml";

	private LinkedBlockingQueue<Bet> queue;

	public Producer(LinkedBlockingQueue<Bet> queue) {
		this.queue = queue;
	}

	@Override
	public void run() {
		super.run();
		try {
			loadBetsFromFile();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void loadBetsFromFile() throws InterruptedException {
		try {
			// File file = new File(homedir, filenameBets);
			File file = new File(filenameBets);

			JAXBContext jaxbContext = JAXBContext.newInstance(Bets.class);

			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();

			Bets bets = (Bets) jaxbUnmarshaller.unmarshal(file);

			addBetsToQueue(bets);

		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	private void addBetsToQueue(Bets bets) throws InterruptedException {
		for (Bet b : bets.getBets()) {
			queue.put(b);
			System.out.println("Bet from file <" + filenameBets
					+ "> added to queue:");
			System.out.println("   " + b.toString());

			sleep(50);
		}
		return;
	}

}
